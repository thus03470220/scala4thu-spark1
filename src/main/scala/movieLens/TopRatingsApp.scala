package movieLens

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac005 on 2017/5/15.
  */
object TopRatingsApp extends App{
  val conf =new SparkConf().setAppName("TopRatings").setMaster("local[*]")
  val sc=new SparkContext(conf)

  val ratings:RDD[(Int,Double)]=sc.textFile("/Users/mac005/Downloads/ml-20m/ratings.csv")
    .map(str=>str.split(","))
    .filter(strs=>strs(1)!="movieId")
    .map(strs=>{
           (strs(1).toInt,strs(2).toDouble)
    })

  //ratings.take(5).foreach(println)

  val totalRatingByMovieId=ratings.reduceByKey((acc,curr)=>acc+curr)
  //totalRatingByMovieId.takeSample(false,5).foreach(println)

  val avergeRatingByMovieId=ratings.mapValues(v=>v->1)
    .reduceByKey((acc,curr)=>{
    (acc._1+curr._1)->(acc._2+curr._2)
  }).mapValues(kv=> kv._1/kv._2.toDouble)

avergeRatingByMovieId.takeSample(false,5).foreach(println)


  val movies=sc.textFile("/Users/mac005/Downloads/ml-20m/movies.csv")
    .map(str=>str.split(","))
      .filter(strs=>strs(0)!="movieId")
    .map(strs=>strs(0).toInt->strs(1))
  movies.take(5).foreach(println)

  val joined=avergeRatingByMovieId.join(movies)
  joined.take(5).foreach(println)
  joined.takeSample(false,5).foreach(println)

  val Top10: Array[(Int, (Double, String))] =joined.sortBy(_._2,false).take(10)
  Top10.foreach(println)

  joined.map(v=>v._1+","+v._2._2+","+v._2._1).saveAsTextFile("result1")


}
